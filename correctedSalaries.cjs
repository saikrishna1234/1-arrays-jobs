let data = require("./1-arrays-jobs.cjs");

data.map((each) => {
  let value = parseFloat(each.salary.slice(1));
  each.corrected_salary = value * 10000;
});

console.log(data);
