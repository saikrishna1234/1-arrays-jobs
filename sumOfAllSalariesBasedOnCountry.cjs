let data = require("./1-arrays-jobs.cjs");

let obj = {};
data.map((each) => {
  if (obj[each.location]) {
    obj[each.location] += parseFloat(each.salary.slice(1));
  } else {
    obj[each.location] = parseFloat(each.salary.slice(1));
  }
});

console.log(obj);
