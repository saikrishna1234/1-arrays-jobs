let data = require("./1-arrays-jobs.cjs");

let obj = {};
data.map((each) => {
  if (obj[each.location]) {
    obj[each.location] += parseFloat(each.salary.slice(1));
  }
  obj[each.location] = parseFloat(each.salary.slice(1));
});

let count = {};
data.map((each) => {
  if (count[each.location]) {
    count[each.location] += 1;
  }
  count[each.location] = 1;
});

let res = {};

for (let i in obj) {
  res[i] = (obj[i] / count[i]).toFixed(2);
}
console.log(res);
