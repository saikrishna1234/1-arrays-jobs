let data = require("./1-arrays-jobs.cjs");

let totalSalary = 0;

data.map((each) => {
  totalSalary += parseFloat(each.salary.slice(1));
});

console.log(totalSalary.toFixed(2));
